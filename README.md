# NHLNow

Mobiiliapplikaatio, jonka päävalikon kautta käyttäjä pääsee näkemään seuraavan kolmen vuorokauden nhl-ottelut, joukkueiden tilastot, sekä pelaajien pelipaikan, numeron sekä nimen.
Applikaatio lataa tiedot HTTP-protokollaa käyttäen Volley-kirjaston avulla JSON-muodossa, josta data parsitaan näytettäväksi applikaatiossa.

Mobiili applikaatiossa hyödynnetään seuraavaa API:a:
* https://github.com/dword4/nhlapi

Linkki Youtubeen, videolla applikaation esittely lyhyesti:
* https://www.youtube.com/watch?v=dRJCvh_BSUs&feature=youtu.be