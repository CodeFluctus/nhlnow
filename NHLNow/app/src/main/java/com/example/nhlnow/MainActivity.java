package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnUpComing = findViewById(R.id.buttonUpComingMatches);
        Button btnTeams = findViewById(R.id.buttonStatsByTeams);
        Button btnPlayers = findViewById(R.id.buttonStatsByPlayers);

        btnUpComing.setOnClickListener(this);
        btnTeams.setOnClickListener(this);
        btnPlayers.setOnClickListener(this);
        VolleyConnector volleyConnector = VolleyConnector.getInstance(this);
        volleyConnector.setUrl("https://statsapi.web.nhl.com/api/v1/standings");
        volleyConnector.updateAllTeamsStatsByUsing("leagueTeams", this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.buttonStatsByPlayers:
                Intent intent = new Intent(this, SelectTeamToInspectActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonStatsByTeams:
                Intent intent2 = new Intent(this, StatsByTeamsActivity.class);
                startActivity(intent2);
                break;
            case R.id.buttonUpComingMatches:
                Intent intent3 = new Intent(this, UpComingMatchesActivity.class);
                startActivity(intent3);
                break;
            default:
                break;
        }
    }
}
