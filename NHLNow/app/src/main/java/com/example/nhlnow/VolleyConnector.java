package com.example.nhlnow;

import android.content.Context;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhlnow.CoreClasses.NHLLeague;
import com.example.nhlnow.CoreClasses.NHLPlayer;
import com.example.nhlnow.CoreClasses.NHLTeam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class VolleyConnector {

    private static VolleyConnector instance;
    private VolleyConnectorInterface callback = null;
    private RequestQueue requestQueue;
    private static Context mcontext;


    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    private static final String TAG_RECORDS = "records";
    private static final String TAG_DIV = "division";
    private static final String TAG_CONF = "conference";
    private static final String TAG_TEAMRECORDS = "teamRecords";
    private static final String TAG_TEAM = "team";
    private static final String TAG_NAME = "name";
    private static final String TAG_LEAGUERECORDS = "leagueRecord";
    private static final String TAG_POINTS = "points";
    private static final String TAG_WINS = "wins";
    private static final String TAG_LOSSES = "losses";
    private static final String TAG_OTLOSSES = "ot";
    private static final String TAG_GP = "gamesPlayed";
    private static final String TAG_DIV_METRO = "Metropolitan";
    private static final String TAG_DIV_ATLANTIC = "Atlantic";
    private static final String TAG_DIV_CENTRAL = "Central";
    private static final String TAG_DIV_PACIFIC = "Pacific";
    private static final String TAG_CONF_EASTERN = "Eastern";
    private static final String TAG_CONF_WESTERN = "Western";

    private ListView listView;
    private String url = "https://statsapi.web.nhl.com/api/v1/standings";
    private ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    private int userRequiredTeamId;
    JSONArray jsonResponse= null;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    interface VolleyConnectorInterface
    {
        void leagueRecordsGathered();
        void allPlayerStatsGathered();
    }

    public void setCallback(VolleyConnectorInterface callback) {
        this.callback = callback;
    }

    private VolleyConnector(Context context) {
        mcontext = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized VolleyConnector getInstance(Context context) {
        if (instance == null) {
            instance = new VolleyConnector(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(mcontext.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {
        getRequestQueue().add(request);
    }


    //-----------------------------------------------------------------
    //-----------------------------------------------------------------


    public void setUrl(String url) {
        this.url = url;
    }

    public void updateAllTeamsStatsByUsing(final String requiredMethod, Context c)
    {
        // Käynnistetään tiedonhaku Volleyn avulla
        JsonObjectRequest dataRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {


                try {
                    // luodaan array joka sisältää tilastotiedot


                    //dataList = GetDivisionTeams(jsonResponse, TAG_DIV_ATLANTIC );
                    //dataList = GetConferenceTeams(jsonResponse, TAG_CONF_EASTERN );

                    if(requiredMethod.equals("leagueTeams"))
                    {
                        //jsonResponse = response.getJSONArray(TAG_RECORDS);
                        /*dataList = */GetLeagueTeams(response);
                    }
                    else if(requiredMethod.equals("PlayerStatsForTeam"))
                    {
                        GetAllPlayerStatsForTeam(response);
                    }

                    /*Log.d("dataList", "size: "+ dataList.size());

                    // sortataan joukkueet pistejärjestykseen suurimmasta pienimpään
                    Collections.sort(dataList, new Comparator<HashMap<String, String>>() {
                        @Override
                        public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                            return o2.get(TAG_POINTS).compareTo(o1.get(TAG_POINTS));
                        }
                    });
                    //luodaan adapteri, jolla saadaan datalistin sisältämä tieto näkymään näytöllä

                    //listView.setAdapter(adapter);

                    Log.d("JPTAG", dataList.toString());*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        //ListAdapter adapter = new SimpleAdapter(c, dataList, R.layout.list_item, new String[] {TAG_TEAM, TAG_GP, TAG_WINS, TAG_LOSSES, TAG_OTLOSSES, TAG_POINTS}, new int[]{ R.id.teamName,R.id.gamesPlayed, R.id.wins, R.id.losses, R.id.otlosses, R.id.teamPoints} );

        //Connector-luokassa on määritelty Volleyn toiminta, jonka avulla haetaan Jsondata nettisivulta
        VolleyConnector.getInstance(c).addToRequestQueue(dataRequest);

        //return adapter;
    }


    private ArrayList<HashMap<String, String>> GetDivisionTeams(JSONArray array, String division) {

        ArrayList<HashMap<String, String>> divTeams = new ArrayList<>();
        JSONObject divisions = new JSONObject();

        try {
            for (int i = 0; i < array.length(); i++) {

                if (array.getJSONObject(i).getJSONObject(TAG_DIV).getString("name").contentEquals(division)) {
                    divisions = array.getJSONObject(i);
                }
            }
            JSONArray tt = divisions.getJSONArray(TAG_TEAMRECORDS);
            for (int j = 0; j < tt.length(); j++) {
                JSONObject teamRecords = tt.getJSONObject(j);
                JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                String name = team.getString(TAG_NAME);

                JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                String wins = leagueRecords.getString(TAG_WINS);
                String losses = leagueRecords.getString(TAG_LOSSES);
                String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                String points = teamRecords.getString(TAG_POINTS);
                String gamesPlayed = teamRecords.getString(TAG_GP);

                HashMap<String, String> teamMap = new HashMap<>();
                teamMap.put(TAG_TEAM, name);
                teamMap.put(TAG_GP, gamesPlayed);
                teamMap.put(TAG_WINS, wins);
                teamMap.put(TAG_LOSSES, losses);
                teamMap.put(TAG_OTLOSSES, otLosses);
                teamMap.put(TAG_POINTS, points);
                divTeams.add(teamMap);
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return divTeams;
    }

    private ArrayList<HashMap<String, String>> GetConferenceTeams(JSONArray array, String conference) {

        ArrayList<HashMap<String, String>> confTeams = new ArrayList<>();

        try {
            JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {

                if (array.getJSONObject(i).getJSONObject(TAG_CONF).getString("name").contentEquals(conference)) {
                    conf.put(array.getJSONObject(i));
                }
            }

            JSONArray tt= new JSONArray();
            for (int i = 0; i < conf.length(); i++) {

                tt = conf.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);

                //tallennetaan uuteen JsonArrayhin yhden joukkeen tiedot kerrallaan

                for (int j = 0; j < tt.length(); j++) {
                    Log.d("TT", "" + tt.getJSONObject(j).toString());
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();
                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    confTeams.add(teamMap);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return confTeams;
    }

    /*
    private ArrayList<HashMap<String, String>> GetLeagueTeams(JSONArray array) {

        ArrayList<HashMap<String, String>> confTeams = new ArrayList<>();

        try {
            //JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {

                //tallennetaan uuteen JsonArrayhin yhden joukkeen tiedot kerrallaan
                JSONArray tt = array.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);
                Log.d("JJP", "onResponse: " + tt.length());
                NHLLeague nhlLeague = NHLLeague.getInstance();

                for (int j = 0; j < tt.length(); j++) {

                    NHLTeam nhlTeam = new NHLTeam();

                    Log.d("TT", "" + tt.getJSONObject(j).toString());
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();
                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    confTeams.add(teamMap);
                    Log.d("List", "GetLeagueTeams: "+ confTeams.size());

                    nhlTeam.setName(name);
                    nhlTeam.setPoints(Integer.parseInt(points));
                    nhlTeam.setGamesPlayed(Integer.parseInt(gamesPlayed));
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put("wins", Integer.parseInt(wins));
                    hashMap.put("losses", Integer.parseInt(losses));
                    hashMap.put("otLosses", Integer.parseInt(otLosses));
                    nhlTeam.setLeagueRecord(hashMap);

                    nhlLeague.addTeamToLeague(nhlTeam);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return confTeams;
    }*/

    private void GetAllPlayerStatsForTeam(JSONObject mainObj)
    {
        Log.d("ASDASD", mainObj.toString());
        try
        {
            JSONArray teamsArray = mainObj.getJSONArray("teams");
            Log.d("ASDASD", teamsArray.toString());
            Log.d("ASDASD", String.valueOf(teamsArray.length()));
            JSONObject firstObjInArray = teamsArray.getJSONObject(0);
            JSONObject rosterObj = firstObjInArray.getJSONObject("roster");
            JSONArray rosterArray = rosterObj.getJSONArray("roster");

            NHLLeague league = NHLLeague.getInstance();
            String teamName = firstObjInArray.getString("name");

            Log.d("ASDASD","TeamName: " + teamName);
            NHLTeam nhlTeam = league.getTeamFromLeagueByName(teamName);

            for (int i = 0; i < rosterArray.length(); i++) {

                NHLPlayer nhlPlayer = new NHLPlayer();

                JSONObject onePlayerData = rosterArray.getJSONObject(i);
                int personId = onePlayerData.getJSONObject("person").getInt("id");
                String fullName = onePlayerData.getJSONObject("person").getString("fullName");
                int jerseyNumber = Integer.parseInt(onePlayerData.getString("jerseyNumber"));
                String positionName = onePlayerData.getJSONObject("position").getString("name");
                String positionAbbreviation = onePlayerData.getJSONObject("position").getString("abbreviation");

                nhlPlayer.setPersonId(personId);
                nhlPlayer.setFullName(fullName);
                nhlPlayer.setJerseyNumber(jerseyNumber);
                nhlPlayer.setPosition(positionName);
                nhlPlayer.setPositionAbbreviation(positionAbbreviation);

                nhlTeam.addPlayerToTeam(nhlPlayer);

                Log.d("ASDASD", "Name: " + fullName + ". ID: " + personId + ". Pelinro: " + jerseyNumber + ".Position: " + positionName + ". Abb: " + positionAbbreviation);
            }

            Log.d("ASDASD", rosterArray.toString());
            Log.d("ASDASD", String.valueOf(rosterArray.length()));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(callback!= null)
        {
            callback.allPlayerStatsGathered();
        }


    }

    private void GetLeagueTeams(JSONObject mainObj) {



        ArrayList<HashMap<String, String>> confTeams = new ArrayList<>();

        try {
            JSONArray array = mainObj.getJSONArray(TAG_RECORDS);
            //JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {
                Log.d("ASDASD", "ARRAYYYY: " + array.toString());
                //Log.d("ASDASD", "ARRAYYYY: " + array.getJSONObject());

                //tallennetaan uuteen JsonArrayhin yhden joukkeen tiedot kerrallaan
                JSONArray tt = array.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);
                Log.d("JJP", "onResponse: " + tt.length());
                Log.d("ASDASD", String.valueOf(i));
                Log.d("ASDASD", "ARRAYYYY: " + array.getJSONObject(i).getJSONObject("division").toString());
                NHLLeague nhlLeague = NHLLeague.getInstance();

                for (int j = 0; j < tt.length(); j++) {

                    NHLTeam nhlTeam = new NHLTeam();

                    Log.d("TT", "" + tt.getJSONObject(j).toString());
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();


                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    confTeams.add(teamMap);
                    Log.d("List", "GetLeagueTeams: "+ confTeams.size());



                    //-----------Added Extra gets------------
                    //--------------- START -----------------
                    nhlTeam.setGoalsAgainst(teamRecords.getInt("goalsAgainst"));
                    nhlTeam.setGoalsScored(teamRecords.getInt("goalsScored"));
                    nhlTeam.setDivisionRank(teamRecords.getInt("divisionRank"));
                    nhlTeam.setConferenceRank(teamRecords.getInt("conferenceRank"));
                    nhlTeam.setTeamId(team.getInt("id"));
                    //Log.d("ASDASD", array.getJSONObject(i).getJSONObject("division").getString("name"));
                    nhlTeam.setDivision(array.getJSONObject(i).getJSONObject("division").getString("name"));

                    //Log.d("ASDASD", array.getJSONObject(i).getJSONObject("conference").getString("name"));
                    nhlTeam.setConference(array.getJSONObject(i).getJSONObject("conference").getString("name"));
                    //----------------- END -------------
                    //-----------Added Extra gets--------


                    nhlTeam.setName(name);
                    nhlTeam.setPoints(Integer.parseInt(points));
                    nhlTeam.setGamesPlayed(Integer.parseInt(gamesPlayed));
                    HashMap<String, Integer> hashMap = new HashMap<>();
                    hashMap.put("wins", Integer.parseInt(wins));
                    hashMap.put("losses", Integer.parseInt(losses));
                    hashMap.put("otLosses", Integer.parseInt(otLosses));
                    nhlTeam.setLeagueRecord(hashMap);

                    nhlLeague.addTeamToLeague(nhlTeam);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        //return confTeams;
        if(callback!= null)
        {
            callback.leagueRecordsGathered();
        }
    }



}

