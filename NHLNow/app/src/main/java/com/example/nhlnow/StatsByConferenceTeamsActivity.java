package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.example.nhlnow.CoreClasses.NHLLeague;
import com.example.nhlnow.CoreClasses.NHLTeam;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class StatsByConferenceTeamsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, VolleyConnector.VolleyConnectorInterface {

    private static final String TAG_RECORDS = "records";
    private static final String TAG_DIV = "division";
    private static final String TAG_CONF = "conference";
    private static final String TAG_TEAMRECORDS = "teamRecords";
    private static final String TAG_TEAM = "team";
    private static final String TAG_NAME = "name";
    private static final String TAG_LEAGUERECORDS = "leagueRecord";
    private static final String TAG_POINTS = "points";
    private static final String TAG_WINS = "wins";
    private static final String TAG_LOSSES = "losses";
    private static final String TAG_OTLOSSES = "ot";
    private static final String TAG_GP = "gamesPlayed";
    private static final String TAG_DIV_METRO = "Metropolitan";
    private static final String TAG_DIV_ATLANTIC = "Atlantic";
    private static final String TAG_DIV_CENTRAL = "Central";
    private static final String TAG_DIV_PACIFIC = "Pacific";
    private static final String TAG_CONF_EASTERN = "Eastern";
    private static final String TAG_CONF_WESTERN = "Western";

    private ListView confListViewEastern;
    private ListView confListViewWestern;
    private String url = "https://statsapi.web.nhl.com/api/v1/standings";
    private ArrayList<HashMap<String, String>> confDataList = new ArrayList<>();
    JSONArray jsonResponse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_by_conference_teams);

        confListViewEastern = findViewById(R.id.confListViewEastern);
        confListViewEastern.setOnItemClickListener(this);
        confListViewWestern = findViewById(R.id.confListViewWestern);
        confListViewWestern.setOnItemClickListener(this);

        updateListViewOfConferenceTeams(TAG_CONF_EASTERN, confListViewEastern);
        updateListViewOfConferenceTeams(TAG_CONF_WESTERN, confListViewWestern);

        ListUtils.setDynamicHeight(confListViewEastern);
        ListUtils.setDynamicHeight(confListViewWestern);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Log.d(TAG, "onItemClick: " + dataList.get(position).get("areaCode"));

        NHLLeague nhlLeague = NHLLeague.getInstance();
        String team = nhlLeague.getTeamFromLeagueByIndex(position).getName();//dataList.get(position).get(TAG_TEAM);
        Log.d("selectedTeam", "onItemClick: " + team);
        //String json = jsonResponse.toString();;
        //Log.d("ASD",);

        //avataan StatsBySingleTeamActivity Intentin avulla ja lähetetään sille samalla tietona maakoodi
        Intent intent = new Intent(this, StatsBySingleTeamActivity.class);
        intent.putExtra("name", team);
        //intent.putExtra("json", json);

        startActivity(intent);
    }

    @Override
    public void leagueRecordsGathered() {
        //updateListViewOfConferenceTeams();
    }

    @Override
    public void allPlayerStatsGathered() {
    }

    public void updateListViewOfConferenceTeams(String conference, ListView conf) {
        NHLLeague league = NHLLeague.getInstance();
        Log.d("ASD", "NHLLeague size: " + league.getNHLLeagueSize());
        ArrayList<HashMap<String, String>> NHLTeamsList = new ArrayList<>();
        ArrayList<NHLTeam> nhlConfTeam = league.getAllNHLTeamsFromConference(conference);

        /*Collections.sort(nhlConfTeam, new Comparator<NHLTeam>() {
            @Override
            public int compare(NHLTeam o1, NHLTeam o2) {
                return Integer.valueOf(o2.getPoints()).compareTo(Integer.valueOf(o1.getPoints()));
            }
        });*/

        for (int i = 0; i < nhlConfTeam.size(); i++) {
            NHLTeam nhlTeam = nhlConfTeam.get(i);
            //NHLTeam nhlTeam = league.getTeamFromLeagueByIndex(i);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(TAG_TEAM, nhlTeam.getName());
            hashMap.put(TAG_GP, Integer.toString(nhlTeam.getGamesPlayed()));
            hashMap.put(TAG_WINS, Integer.toString(nhlTeam.getLeagueRecord().get("wins")));
            hashMap.put(TAG_LOSSES, Integer.toString(nhlTeam.getLeagueRecord().get("losses")));
            hashMap.put(TAG_OTLOSSES, Integer.toString(nhlTeam.getLeagueRecord().get("otLosses")));
            hashMap.put(TAG_POINTS, Integer.toString(nhlTeam.getPoints()));
            NHLTeamsList.add(hashMap);
        }

        String from[] = {TAG_TEAM, TAG_GP, TAG_WINS, TAG_LOSSES, TAG_OTLOSSES, TAG_POINTS};
        int to[] = {R.id.teamName,R.id.gamesPlayed, R.id.wins, R.id.losses, R.id.otlosses, R.id.teamPoints};

        ListAdapter adapter = new SimpleAdapter(this, NHLTeamsList, R.layout.list_item, from, to);

        conf.setAdapter(adapter);
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();

            }
            Log.d("Measure", "setDynamicHeight: " + mListAdapter.getCount() + ", " + height);
            Log.d("Measure", "setDynamicHeight: " + mListView.getDividerHeight());
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height-400;// + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
