package com.example.nhlnow.CoreClasses;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class NHLLeague {

    private static NHLLeague singleton = null;
    private ArrayList<NHLTeam> origAllNHLTeams = new ArrayList<>();
    private ArrayList<NHLTeam> allNHLTeams = new ArrayList<>();



    private NHLLeague(){
        origAllNHLTeams = allNHLTeams;
    }


    public static NHLLeague getInstance()
    {
        if(singleton == null)
        {
            singleton = new NHLLeague();
        }
        return singleton;
    }


    public void addTeamToLeague(NHLTeam team)
    {
        if(!haveTeamNamed(team.getName()))
        {
            allNHLTeams.add(team);
        }
    }

    public NHLTeam getTeamFromLeagueByIndex(int i)
    {
        if(i >= 0 && i < allNHLTeams.size())
        {
            return allNHLTeams.get(i);
        }
        else
        {
            return null;
        }
    }

    public int getNHLLeagueSize()
    {
        return allNHLTeams.size();
    }

    public NHLTeam getTeamFromLeagueByName(String name)
    {
        for (int i = 0; i < allNHLTeams.size(); i++)
        {
            if(allNHLTeams.get(i).getName().equals(name))
            {
                return allNHLTeams.get(i);
            }
        }
        return null;
    }

    public ArrayList<NHLTeam> getAllNHLTeamsFromDivision(String divisionName)
    {
        ArrayList<NHLTeam> allDivisionTeams = new ArrayList<>();
        for(int i = 0; i < allNHLTeams.size(); i++)
        {
            NHLTeam team = allNHLTeams.get(i);
            if(team.getDivision().equals(divisionName))
            {
                allDivisionTeams.add(team);
            }
        }
        return allDivisionTeams;
    }

    public ArrayList<NHLTeam> getAllNHLTeamsFromConference(String conferenceName)
    {
        ArrayList<NHLTeam> allConferenceTeams = new ArrayList<>();
        for(int i = 0; i < origAllNHLTeams.size(); i++)
        {
            NHLTeam team = origAllNHLTeams.get(i);
            if(team.getConference().equals(conferenceName))
            {
                allConferenceTeams.add(team);
            }
        }
        return allConferenceTeams;
    }

    private boolean haveTeamNamed(String teamName)
    {
        for(int i = 0; i < allNHLTeams.size(); i++)
        {
            if(teamName.equals(getTeamFromLeagueByIndex(i).getName()))
            {
                return true;
            }
        }
        return false;
    }

    public void orderTeamsByPoints()
    {
        Collections.sort(allNHLTeams, new Comparator<NHLTeam>() {
            @Override
            public int compare(NHLTeam o1, NHLTeam o2) {
                return Integer.valueOf(o2.getPoints()).compareTo(Integer.valueOf(o1.getPoints()));
            }
        });
        /*
        ArrayList<NHLTeam> teamOrderedByPoints = new ArrayList<>();

        for (int i = 0; i < allNHLTeams.size(); i++)
        {
            NHLTeam teamFromOriginal = allNHLTeams.get(i);
            int origTeamPoints = teamFromOriginal.getPoints();
            if(i == 0)
            {
                Log.d("ASD:", "FIRST TEAM ADDED");
                teamOrderedByPoints.add(teamFromOriginal);
            }
            else
            {
                Log.d("ASD:", "OrigTeamPoints: " + origTeamPoints);
                Log.d("ASD:", "tOBP.size(): " + teamOrderedByPoints.size());
                for (int j = 0; j < teamOrderedByPoints.size(); j++) {
                    Log.d("ASD:", "tOBP: " + teamOrderedByPoints.get(j).getPoints());
                    if(origTeamPoints > teamOrderedByPoints.get(j).getPoints())
                    {
                        teamOrderedByPoints.add(j,teamFromOriginal);
                        break;
                    }
                    else
                    {
                        teamOrderedByPoints.add(teamFromOriginal);
                        break;
                    }
                }
            }
            Log.d("ASD:", "STARTING NEW ROUND");
        }

        allNHLTeams = teamOrderedByPoints;
    */
    }
}
