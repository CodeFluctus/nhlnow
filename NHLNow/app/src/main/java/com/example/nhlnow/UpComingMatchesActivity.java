package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewDebug;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class UpComingMatchesActivity extends AppCompatActivity {

    // https://statsapi.web.nhl.com/api/v1/schedule?startDate=2020-02-20&endDate=2020-02-22

    private static final String TAG_DATES = "dates";
    private static final String TAG_DATE = "date";
    private static final String TAG_GAMES = "games";
    private static final String TAG_GAMEDATE = "gameDate";
    private static final String TAG_TEAMS = "teams";
    private static final String TAG_AWAY = "away";
    private static final String TAG_TEAM = "team";
    private static final String TAG_NAME = "name";
    private static final String TAG_ID = "id";
    private static final String TAG_IDHOMEDRAWABLE = "idhome";
    private static final String TAG_IDAWAYDRAWABLE = "idaway";
    private static final String TAG_HOME = "home";
    private static final String TAG_VENUE = "venue";

    private String startDate = getStartDate(); // ESIM "2020-02-20"
    private String endDate = getEndDate();   // ESIM "2020-02-22"

    private ListView listView;
    private String url = "https://statsapi.web.nhl.com/api/v1/schedule?startDate=" + startDate + "&endDate=" + endDate;
    private ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    JSONArray jsonResponse= null;

    private static final String TAG = "UpComingMatchesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_coming_matches);

        listView = findViewById(R.id.listViewUpComing);

        // Käynnistetään tiedonhaku Volleyn avulla
        JsonObjectRequest dataRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    // luodaan array joka sisältää tilastotiedot
                    jsonResponse = response.getJSONArray(TAG_DATES);

                    //dataList = GetDivisionTeams(jsonResponse, TAG_DIV_ATLANTIC );
                    //dataList = GetConferenceTeams(jsonResponse, TAG_CONF_EASTERN );
                    dataList = GetSchedule(jsonResponse);
                    Log.d("dataList", "size: "+ dataList.size());

                    // sortataan joukkueet pistejärjestykseen suurimmasta pienimpään
                    /*Collections.sort(dataList, new Comparator<HashMap<String, String>>() {
                        @Override
                        public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                            return o2.get(TAG_POINTS).compareTo(o1.get(TAG_POINTS));
                        }
                    });*/
                    //luodaan adapteri, jolla saadaan datalistin sisältämä tieto näkymään näytöllä
                    ListAdapter adapter = new SimpleAdapter(UpComingMatchesActivity.this, dataList, R.layout.list_item_home_vs_away, new String[] {TAG_DATE, TAG_HOME, TAG_AWAY, TAG_IDAWAYDRAWABLE, TAG_IDHOMEDRAWABLE}, new int[]{R.id.textViewGameDate, R.id.textViewHome,R.id.textViewAway, R.id.imageViewHome, R.id.imageViewAway});
                    listView.setAdapter(adapter);

                    Log.d("JPTAG", dataList.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        //Connector-luokassa on määritelty Volleyn toiminta, jonka avulla haetaan Jsondata nettisivulta
        VolleyConnector.getInstance(this).addToRequestQueue(dataRequest);

    }

    public ArrayList<HashMap<String, String>> GetSchedule(JSONArray array) {

        ArrayList<HashMap<String, String>> schedule = new ArrayList<>();

        try {
            //JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {

                String date = array.getJSONObject(i).getString(TAG_DATE);
                Log.d(TAG, "GetSchedule: date = " + date);                                     // date

                //Tallennetaan JSONArrayhin yhden vuorokauden pelitiedot kerrallaan
                JSONArray gamesArr = array.getJSONObject(i).getJSONArray(TAG_GAMES);
                for (int j = 0; j < gamesArr.length() ; j++) {

                    Log.d(TAG, "GetSchedule: GAME number " + (j+1) + ".");
                    String gameDate = gamesArr.getJSONObject(j).getString(TAG_GAMEDATE);
                    Log.d(TAG, "GetSchedule: GAME date: " + gameDate);                         // gameDate

                    //Noudetaan vieras- ja kotijoukkueet
                    JSONObject teamsObj = gamesArr.getJSONObject(j).getJSONObject(TAG_TEAMS);

                    JSONObject awayObj = teamsObj.getJSONObject(TAG_AWAY).getJSONObject(TAG_TEAM);
                    String awayTeam = awayObj.getString(TAG_NAME);                                  // awayTeam
                    String awayID = awayObj.getString(TAG_ID);                                      // awayID
                    Log.d(TAG, "GetSchedule: GAME away team: " + awayTeam);

                    JSONObject homeObj = teamsObj.getJSONObject(TAG_HOME).getJSONObject(TAG_TEAM);
                    String homeTeam = homeObj.getString(TAG_NAME);                                  // homeTeam
                    String homeID = homeObj.getString(TAG_ID);                                      // homeID
                    Log.d(TAG, "GetSchedule: GAME home team: " + homeTeam);

                    //Noudetaan resurssi-id kotijoukkueen kuvalle
                    String uriHomeTeam = "@drawable/l"+ homeID;
                    //Log.d(TAG, "GetSchedule: " + uriHomeTeam);
                    int resIdHome = getResources().getIdentifier(uriHomeTeam, null, getPackageName());
                    String resIdHomeString = Integer.toString(resIdHome);
                    Log.d(TAG, "GetSchedule: " + resIdHomeString);

                    //Noudetaan resurssi-id vierasjoukkueen kuvalle
                    String uriAwayTeam = "@drawable/l"+ awayID;
                    //Log.d(TAG, "GetSchedule: " + uriAwayTeam);
                    int resIdAway = getResources().getIdentifier(uriAwayTeam, null, getPackageName());
                    String resIdAwayString = Integer.toString(resIdAway);
                    Log.d(TAG, "GetSchedule: " + resIdAwayString);

                    HashMap<String, String> gameMap = new HashMap<>();
                    gameMap.put(TAG_DATE, date);
                    gameMap.put(TAG_GAMEDATE, gameDate);
                    gameMap.put(TAG_AWAY, awayTeam);
                    gameMap.put(TAG_IDAWAYDRAWABLE, resIdHomeString);
                    gameMap.put(TAG_HOME, homeTeam);
                    gameMap.put(TAG_IDHOMEDRAWABLE, resIdAwayString);
                    schedule.add(gameMap);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return schedule;
    }

    public String getStartDate() {

        int YEAR = Calendar.getInstance().get(Calendar.YEAR);
        int MONTH = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int DAY_OF_MONTH = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        String TRUEMONTH = Integer.toString(MONTH);

        if(MONTH < 10) {
            TRUEMONTH = "0" + TRUEMONTH;
        }

        String startDate = "" + YEAR + "-" + TRUEMONTH + "-" + DAY_OF_MONTH;
        Log.d("getStartDate", "getStartDate: " + startDate);

        return startDate;
    }

    public String getEndDate() {

        int YEAR = Calendar.getInstance().get(Calendar.YEAR);
        int MONTH = Calendar.getInstance().get(Calendar.MONTH) + 1;
        int DAY_OF_MONTH = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) + 2;

        String TRUEMONTH = Integer.toString(MONTH);

        if(MONTH < 10) {
            TRUEMONTH = "0" + TRUEMONTH;
        }

        String endDate = "" + YEAR + "-" + TRUEMONTH + "-" + DAY_OF_MONTH;
        Log.d("getEndDate", "getEndDate: " + endDate);

        return endDate;
    }

}

