package com.example.nhlnow.CoreClasses;


public class NHLPlayer {
    private String firstName;
    private String lastName;
    private String fullName;
    private String position;
    private String positionAbbreviation;
    private int jerseyNumber;
    private int personId;
    private int age;
    private int points;
    private int assists, goals, pim, shots, games;
    private int powerPlayGoals, powerPlayPoints;
    private int penaltyMinutes;
    private float shotPct;
    private int gameWinningGoals, overTimeGoals;
    private int shortHandendGoals, shortHandedPoints;


    public int getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionAbbreviation() {
        return positionAbbreviation;
    }

    public void setPositionAbbreviation(String positionAbbreviation) {
        this.positionAbbreviation = positionAbbreviation;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getPim() {
        return pim;
    }

    public void setPim(int pim) {
        this.pim = pim;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getPowerPlayGoals() {
        return powerPlayGoals;
    }

    public void setPowerPlayGoals(int powerPlayGoals) {
        this.powerPlayGoals = powerPlayGoals;
    }

    public int getPowerPlayPoints() {
        return powerPlayPoints;
    }

    public void setPowerPlayPoints(int powerPlayPoints) {
        this.powerPlayPoints = powerPlayPoints;
    }

    public int getPenaltyMinutes() {
        return penaltyMinutes;
    }

    public void setPenaltyMinutes(int penaltyMinutes) {
        this.penaltyMinutes = penaltyMinutes;
    }

    public float getShotPct() {
        return shotPct;
    }

    public void setShotPct(float shotPct) {
        this.shotPct = shotPct;
    }

    public int getGameWinningGoals() {
        return gameWinningGoals;
    }

    public void setGameWinningGoals(int gameWinningGoals) {
        this.gameWinningGoals = gameWinningGoals;
    }

    public int getOverTimeGoals() {
        return overTimeGoals;
    }

    public void setOverTimeGoals(int overTimeGoals) {
        this.overTimeGoals = overTimeGoals;
    }

    public int getShortHandendGoals() {
        return shortHandendGoals;
    }

    public void setShortHandendGoals(int shortHandendGoals) {
        this.shortHandendGoals = shortHandendGoals;
    }

    public int getShortHandedPoints() {
        return shortHandedPoints;
    }

    public void setShortHandedPoints(int shortHandedPoints) {
        this.shortHandedPoints = shortHandedPoints;
    }
}
