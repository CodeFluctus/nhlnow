package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.nhlnow.CoreClasses.NHLLeague;
import com.example.nhlnow.CoreClasses.NHLPlayer;
import com.example.nhlnow.CoreClasses.NHLTeam;

import java.util.ArrayList;
import java.util.HashMap;

public class StatsByPlayersActivity extends AppCompatActivity implements View.OnClickListener, VolleyConnector.VolleyConnectorInterface {

    private ImageView teamLogo;
    private TextView teamName, teamDivision, teamConference;
    private ListView playersStatsLV;
    private NHLTeam nhlTeam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_by_players);


        teamLogo = findViewById(R.id.slogo);
        teamName = findViewById(R.id.singleTeamName);
        teamDivision = findViewById(R.id.singleDivision);
        teamConference = findViewById(R.id.singleConference);
        playersStatsLV = findViewById(R.id.teamPlayerListView);


        String teamNameString = null;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            teamNameString = extras.getString("TEAMNAME");
            //json = extras.getString("json");
            //Log.d("parse", "onCreate: " + teamName);
        }

        NHLLeague league = NHLLeague.getInstance();

        nhlTeam = league.getTeamFromLeagueByName(teamNameString);

        VolleyConnector volleyConnector = VolleyConnector.getInstance(this);
        String urlStringForTeamStats = "https://statsapi.web.nhl.com/api/v1/teams/" + nhlTeam.getTeamId() + "/?expand=team.roster";
        volleyConnector.setUrl(urlStringForTeamStats);
        volleyConnector.setCallback(this);
        volleyConnector.updateAllTeamsStatsByUsing("PlayerStatsForTeam", this);

        String uri = "@drawable/l"+ nhlTeam.getTeamId();
        int resId = getResources().getIdentifier(uri, null, getPackageName()); Drawable drawable = getResources().getDrawable(resId);
        teamLogo.setImageDrawable(drawable);

        teamName.setText(nhlTeam.getName());
        teamConference.setText(nhlTeam.getConference());
        teamDivision.setText(nhlTeam.getDivision());

        updateView();


    }

    @Override
    public void leagueRecordsGathered() {

    }

    @Override
    public void allPlayerStatsGathered() {
        updateView();
    }

    @Override
    public void onClick(View v) {

        /*int id = v.getId();
        switch(id)
        {
            case R.id.teamButtonAnaheimDucks:
            {
                Log.d("ASD", "Clicked1!");
                break;
            }
            case R.id.teamButtonArizonaCoyotes:
            {
                Log.d("ASD", "Clicked2!");
                break;
            }
            case R.id.teamButtonBostonBruins:
            {
                Log.d("ASD", "Clicked3!");
                break;
            }

            default:
                break;
        }*/
    }

    private void updateView()
    {
        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
        nhlTeam.orderPlayersByPosition();
        //Log.d("ASDASD", "TRIGGERED!");

        for(int i = 0; i < nhlTeam.getNHLTeamSize(); i++)
        {
            NHLPlayer player = nhlTeam.getPlayerFromTeamByIndex(i);

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("FullName", player.getFullName());
            hashMap.put("Position", player.getPosition());
            hashMap.put("ABRV", player.getPositionAbbreviation());
            hashMap.put("JerseyNumber", Integer.toString(player.getJerseyNumber()));
            //Log.d("ASDASD", player.getFullName()+ " " + player.getPosition()+ " " + player.getPositionAbbreviation() + " " + player.getJerseyNumber());
            arrayList.add(hashMap);
        }

        //Creating String array that includes keyvalues to HashMap
        String from[] = {"FullName" , "Position" , "ABRV", "JerseyNumber"};

        //Creating Int array that includes id's where to initialize each key's pointed values
        int to[] = {R.id.playerNameTextView, R.id.playerPositionTextView, R.id.playerPosAbbTextView, R.id.playerJNTextView};

        final SimpleAdapter adapter = new SimpleAdapter(this, arrayList, R.layout.custom_row_for_teamplayers, from, to);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playersStatsLV.setAdapter(adapter);
            }
        });
    }
}
