package com.example.nhlnow.CoreClasses;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class NHLTeam {

    private int teamId;
    private String name;
    private String division;
    private String conference;

    private int leagueRank;
    private int divisionRank;
    private int conferenceRank;

    private int points;
    private int gamesPlayed;
    private int goalsAgainst;
    private int goalsScored;

    private HashMap<String, Integer> leagueRecord;

    private ArrayList<NHLPlayer> allTeamPlayers;

    public NHLTeam()
    {
        //These key values must be used while a inserting values.
        /*leagueRecord.put("wins", null);
        leagueRecord.put("losses", null);
        leagueRecord.put("otLosses", null);*/
        allTeamPlayers = new ArrayList<>();
    }

    public void addPlayerToTeam(NHLPlayer player)
    {
        if (!hasPlayerWithId(player.getPersonId()))
        {
            allTeamPlayers.add(player);
        }
    }

    public NHLPlayer getPlayerFromTeamByIndex(int i)
    {
        if(i >= 0 && i < allTeamPlayers.size())
        {
            return allTeamPlayers.get(i);
        }
        else
        {
            return null;
        }
    }

    public int getNHLTeamSize()
    {
        return allTeamPlayers.size();
    }

    public int getTeamId() {
        return teamId;
    }

    public ArrayList<NHLPlayer> getAllTeamPlayers() {
        return allTeamPlayers;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
        Log.d("NHLTEAM", "Team SET DIVISION: " + this.division);
    }

    public String getConference() {
        return conference;
    }

    public void setConference(String conference) {
        this.conference = conference;
    }

    public int getLeagueRank() {
        return leagueRank;
    }

    public void setLeagueRank(int leagueRank) {
        this.leagueRank = leagueRank;
    }

    public int getDivisionRank() {
        return divisionRank;
    }

    public void setDivisionRank(int divisionRank) {
        this.divisionRank = divisionRank;
    }

    public int getConferenceRank() {
        return conferenceRank;
    }

    public void setConferenceRank(int conferenceRank) {
        this.conferenceRank = conferenceRank;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalsScored() {
        return goalsScored;
    }

    public void setGoalsScored(int goalsScored) {
        this.goalsScored = goalsScored;
    }

    public HashMap<String, Integer> getLeagueRecord() {
        return leagueRecord;
    }

    public void setLeagueRecord(HashMap<String, Integer> leagueRecord) {
        this.leagueRecord = leagueRecord;
    }

    private boolean hasPlayerWithId(int personId)
    {
        for (int i = 0; i < getNHLTeamSize(); i++) {
            if(personId == allTeamPlayers.get(i).getPersonId())
            {
                return true;
            }
        }
        return false;
    }

    public void orderPlayersByPosition() {

        ArrayList<NHLPlayer> objAllPlayers = new ArrayList<>();
        String[] requiredABRV =  {"G", "D", "LW", "RW", "C"};
        for (int i = 0; i < requiredABRV.length; i++)
        {
            ArrayList<NHLPlayer> requiredABRVArrayList = getPlayerByABRV(requiredABRV[i]);
            for (int j = 0; j < requiredABRVArrayList.size(); j++) {
                objAllPlayers.add(requiredABRVArrayList.get(j));
            }
        }
        this.allTeamPlayers = objAllPlayers;
    }

    private ArrayList<NHLPlayer> getPlayerByABRV(String abrv)
    {
        ArrayList<NHLPlayer> playersWithRequiredAbrv = new ArrayList<>();

        for (int i = 0; i < allTeamPlayers.size(); i++) {
            NHLPlayer objPlayer = allTeamPlayers.get(i);
            String objPlayerABRV = objPlayer.getPositionAbbreviation();
            if(abrv.equals(objPlayerABRV))
            {
                playersWithRequiredAbrv.add(objPlayer);
            }
        }
        return playersWithRequiredAbrv;
    }
}
