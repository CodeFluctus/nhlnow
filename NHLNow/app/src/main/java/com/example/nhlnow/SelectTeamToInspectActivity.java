package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.nhlnow.CoreClasses.NHLLeague;

public class SelectTeamToInspectActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_team_to_inspect);

        //Setting onClickListeners for every button.

        findViewById(R.id.teamButtonAnaheimDucks).setOnClickListener(this);
        findViewById(R.id.teamButtonArizonaCoyotes).setOnClickListener(this);
        findViewById(R.id.teamButtonBostonBruins).setOnClickListener(this);
        findViewById(R.id.teamButtonBuffaloSabres).setOnClickListener(this);
        findViewById(R.id.teamButtonCalgaryFlames).setOnClickListener(this);

        findViewById(R.id.teamButtonCarolinaHurricanes).setOnClickListener(this);
        findViewById(R.id.teamButtonChicagoBlackhawks).setOnClickListener(this);
        findViewById(R.id.teamButtonColoradoAvalanche).setOnClickListener(this);
        findViewById(R.id.teamButtonColumbusBlueJackets).setOnClickListener(this);
        findViewById(R.id.teamButtonDallasStars).setOnClickListener(this);

        findViewById(R.id.teamButtonDetroitRedWings).setOnClickListener(this);
        findViewById(R.id.teamButtonEdmontonOilers).setOnClickListener(this);
        findViewById(R.id.teamButtonFloridaPanthers).setOnClickListener(this);
        findViewById(R.id.teamButtonLosAngelesKings).setOnClickListener(this);

        findViewById(R.id.teamButtonMinnesotaWild).setOnClickListener(this);
        findViewById(R.id.teamButtonMontrealCanadiens).setOnClickListener(this);
        findViewById(R.id.teamButtonNashvillePredators).setOnClickListener(this);
        findViewById(R.id.teamButtonNewJerseyDevils).setOnClickListener(this);

        findViewById(R.id.teamButtonNewYorkIslanders).setOnClickListener(this);
        findViewById(R.id.teamButtonNewYorkRangers).setOnClickListener(this);
        findViewById(R.id.teamButtonOttawaSenators).setOnClickListener(this);
        findViewById(R.id.teamButtonPhiladelphiaFlyers).setOnClickListener(this);

        findViewById(R.id.teamButtonPittsburghPenguins).setOnClickListener(this);
        findViewById(R.id.teamButtonStLouisBlues).setOnClickListener(this);
        findViewById(R.id.teamButtonSanJoseSharks).setOnClickListener(this);
        findViewById(R.id.teamButtonTampaBayLightning).setOnClickListener(this);

        findViewById(R.id.teamButtonTorontoMapleLeafs).setOnClickListener(this);
        findViewById(R.id.teamButtonVancouverCanucks).setOnClickListener(this);
        findViewById(R.id.teamButtonVegasGoldenKnights).setOnClickListener(this);
        findViewById(R.id.teamButtonWashingtonCapitals).setOnClickListener(this);

        findViewById(R.id.teamButtonWinnipegJets).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(this, StatsByPlayersActivity.class);

        if(v.getId() == R.id.teamButtonAnaheimDucks)
        {
            intent.putExtra("TEAMNAME", "Anaheim Ducks");
        }
        else if(v.getId() == R.id.teamButtonArizonaCoyotes)
        {
            intent.putExtra("TEAMNAME", "Arizona Coyotes");
        }
        else if(v.getId() == R.id.teamButtonBostonBruins)
        {
            intent.putExtra("TEAMNAME", "Boston Bruins");
        }
        else if(v.getId() == R.id.teamButtonBuffaloSabres)
        {
            intent.putExtra("TEAMNAME", "Buffalo Sabres");
        }
        else if(v.getId() == R.id.teamButtonCalgaryFlames)
        {
            intent.putExtra("TEAMNAME", "Calgary Flames");
        }
        else if(v.getId() == R.id.teamButtonCarolinaHurricanes)
        {
            intent.putExtra("TEAMNAME", "Carolina Hurricanes");
        }
        else if(v.getId() == R.id.teamButtonChicagoBlackhawks)
        {
            intent.putExtra("TEAMNAME", "Chicago Blackhawks");
        }
        else if(v.getId() == R.id.teamButtonColoradoAvalanche)
        {
            intent.putExtra("TEAMNAME", "Colorado Avalanche");
        }
        else if(v.getId() == R.id.teamButtonColumbusBlueJackets)
        {
            intent.putExtra("TEAMNAME", "Columbus Blue Jackets");
        }
        else if(v.getId() == R.id.teamButtonDallasStars)
        {
            intent.putExtra("TEAMNAME", "Dallas Stars");
        }
        else if(v.getId() == R.id.teamButtonDetroitRedWings)
        {
            intent.putExtra("TEAMNAME", "Detroit Red Wings");
        }
        else if(v.getId() == R.id.teamButtonEdmontonOilers)
        {
            intent.putExtra("TEAMNAME", "Edmonton Oilers");
        }
        else if(v.getId() == R.id.teamButtonFloridaPanthers)
        {
            intent.putExtra("TEAMNAME", "Florida Panthers");
        }
        else if(v.getId() == R.id.teamButtonLosAngelesKings)
        {
            intent.putExtra("TEAMNAME", "Los Angeles Kings");
        }
        else if(v.getId() == R.id.teamButtonMinnesotaWild)
        {
            intent.putExtra("TEAMNAME", "Minnesota Wild");
        }
        else if(v.getId() == R.id.teamButtonMontrealCanadiens)
        {
            intent.putExtra("TEAMNAME", "Montréal Canadiens");
        }
        else if(v.getId() == R.id.teamButtonNashvillePredators)
        {
            intent.putExtra("TEAMNAME", "Nashville Predators");
        }
        else if(v.getId() == R.id.teamButtonNewJerseyDevils)
        {
            intent.putExtra("TEAMNAME", "New Jersey Devils");
        }
        else if(v.getId() == R.id.teamButtonNewYorkIslanders)
        {
            intent.putExtra("TEAMNAME", "New York Islanders");
        }
        else if(v.getId() == R.id.teamButtonNewYorkRangers)
        {
            intent.putExtra("TEAMNAME", "New York Rangers");
        }
        else if(v.getId() == R.id.teamButtonOttawaSenators)
        {
            intent.putExtra("TEAMNAME", "Ottawa Senators");
        }
        else if(v.getId() == R.id.teamButtonPhiladelphiaFlyers)
        {
            intent.putExtra("TEAMNAME", "Philadelphia Flyers");
        }
        else if(v.getId() == R.id.teamButtonPittsburghPenguins)
        {
            intent.putExtra("TEAMNAME", "Pittsburgh Penguins");
        }
        else if(v.getId() == R.id.teamButtonStLouisBlues)
        {
            intent.putExtra("TEAMNAME", "St. Louis Blues");
        }
        else if(v.getId() == R.id.teamButtonSanJoseSharks)
        {
            intent.putExtra("TEAMNAME", "San Jose Sharks");
        }
        else if(v.getId() == R.id.teamButtonTampaBayLightning)
        {
            intent.putExtra("TEAMNAME", "Tampa Bay Lightning");
        }
        else if(v.getId() == R.id.teamButtonTorontoMapleLeafs)
        {
            intent.putExtra("TEAMNAME", "Toronto Maple Leafs");
        }
        else if(v.getId() == R.id.teamButtonVancouverCanucks)
        {
            intent.putExtra("TEAMNAME", "Vancouver Canucks");
        }
        else if(v.getId() == R.id.teamButtonVegasGoldenKnights)
        {
            intent.putExtra("TEAMNAME", "Vegas Golden Knights");
        }
        else if(v.getId() == R.id.teamButtonWashingtonCapitals)
        {
            intent.putExtra("TEAMNAME", "Washington Capitals");
        }
        else if(v.getId() == R.id.teamButtonWinnipegJets)
        {
            intent.putExtra("TEAMNAME", "Winnipeg Jets");
        }

        startActivity(intent);


        /*int id = v.getId();
        switch(id)
        {
            case R.id.teamButtonAnaheimDucks:
            {
                Log.d("ASD", "Clicked1!");
                break;
            }
            case R.id.teamButtonArizonaCoyotes:
            {
                Log.d("ASD", "Clicked2!");
                break;
            }
            case R.id.teamButtonBostonBruins:
            {
                Log.d("ASD", "Clicked3!");
                break;
            }

            default:
                break;
        }*/

    }
}
