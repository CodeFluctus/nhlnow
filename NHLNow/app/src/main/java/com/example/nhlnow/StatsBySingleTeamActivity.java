package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nhlnow.CoreClasses.NHLLeague;
import com.example.nhlnow.CoreClasses.NHLTeam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

public class StatsBySingleTeamActivity extends AppCompatActivity {

    private static final String TAG_DIV = "division";
    private static final String TAG_CONF = "conference";
    private static final String TAG_TEAMRECORDS = "teamRecords";
    private static final String TAG_TEAM = "team";
    private static final String TAG_NAME = "name";
    private static final String TAG_LEAGUERECORDS = "leagueRecord";
    private static final String TAG_POINTS = "points";
    private static final String TAG_WINS = "wins";
    private static final String TAG_LOSSES = "losses";
    private static final String TAG_OTLOSSES = "ot";
    private static final String TAG_GP = "gamesPlayed";
    private static final String TAG_GF = "goalsScored";
    private static final String TAG_GA = "goalsAgainst";
    private static final String TAG_DIVRANK = "divisionRank";
    private static final String TAG_CONFRANK = "conferenceRank";
    private static final String TAG_ID = "id";

    private String teamName = "";
    private String json = "";
    private JSONArray teamArray;
    private ArrayList<HashMap<String, String>> teamList = new ArrayList<>();
    private String teamDiv = "";
    private String teamConf = "";
    private TextView sTeamName;
    private TextView sGp;
    private TextView sWins;
    private TextView sLosses;
    private TextView sOtLosses;
    private TextView sPoints;
    private TextView sGoalsFor;
    private TextView sGoalsAgainst;
    private TextView sDifference;
    private TextView sDivRank;
    private TextView sConfRank;
    private ImageView slogo;
    private TextView sDivision;
    private TextView sConference;
    private Button rosterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_by_single_team);

        slogo = findViewById(R.id.slogo);
        sTeamName = findViewById(R.id.singleTeamName);
        sGp = findViewById(R.id.singleGamesPlayed);
        sWins = findViewById(R.id.singleWins);
        sLosses = findViewById(R.id.singleLosses);
        sOtLosses = findViewById(R.id.singleOtlosses);
        sPoints = findViewById(R.id.singleTeamPoints);
        sGoalsFor = findViewById(R.id.singleGoalsFor);
        sGoalsAgainst = findViewById(R.id.singleGoalsAgainst);
        sDifference = findViewById(R.id.singleDiff);
        sDivRank = findViewById(R.id.singleDivRank);
        sConfRank = findViewById(R.id.singleConfRank);
        sDivision = findViewById(R.id.singleDivision);
        sConference = findViewById(R.id.singleConference);
        rosterButton = findViewById(R.id.rosterButton);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            teamName = extras.getString("name");
            //json = extras.getString("json");
            //Log.d("parse", "onCreate: " + teamName);
        }


        NHLLeague nhlLeague = NHLLeague.getInstance();

        final NHLTeam nhlTeam = nhlLeague.getTeamFromLeagueByName(teamName);

        Log.d("ASD", nhlTeam.getName());


        String uri = "@drawable/l"+ nhlTeam.getTeamId();
        int resId = getResources().getIdentifier(uri, null, getPackageName());
        Drawable drawable = getResources().getDrawable(resId);

        slogo.setImageDrawable(drawable);

        sTeamName.setText(nhlTeam.getName());
        sGp.setText(String.valueOf(nhlTeam.getGamesPlayed()));
        sWins.setText(String.valueOf(nhlTeam.getLeagueRecord().get("wins")));
        sLosses.setText(String.valueOf(nhlTeam.getLeagueRecord().get("losses")));
        sOtLosses.setText(String.valueOf(nhlTeam.getLeagueRecord().get("otLosses")));
        sPoints.setText(String.valueOf(nhlTeam.getPoints()));
        sGoalsFor.setText(String.valueOf(nhlTeam.getGoalsScored()));
        sGoalsAgainst.setText(String.valueOf(nhlTeam.getGoalsAgainst()));
        int goalDiff = nhlTeam.getGoalsScored() - nhlTeam.getGoalsAgainst();
        sDifference.setText(String.valueOf(goalDiff));

        sDivRank.setText(String.valueOf(nhlTeam.getDivisionRank()));
        sConfRank.setText(String.valueOf(nhlTeam.getConferenceRank()));
        sDivision.setText(nhlTeam.getDivision());
        sConference.setText(nhlTeam.getConference());

        rosterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StatsBySingleTeamActivity.this, StatsByPlayersActivity.class);
                intent.putExtra("TEAMNAME", nhlTeam.getName());
                startActivity(intent);
            }
        });
    }


        /*Bundle extras = getIntent().getExtras();
        if (extras != null) {
            teamName = extras.getString("name");
            json = extras.getString("json");
            //Log.d("parse", "onCreate: " + teamName);
        }

        try {
            teamArray = new JSONArray(json);
            //Log.d("parse", "onCreate: " + teamArray.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }

        teamList = GetTeamInfo(teamArray, teamName);

    }

    public ArrayList<HashMap<String, String>> GetTeamInfo(JSONArray array, String wantedTeam) {

        ArrayList<HashMap<String, String>> teamInfo = new ArrayList<>();
        JSONObject selectedDiv = new JSONObject();

        try {
            for (int i = 0; i < array.length(); i++) {
                JSONArray leagueTeams = array.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);
                //Log.d("leagueTeams", "GetTeamInfo: " + leagueTeams.length());
                //if (array.getJSONObject(i).getJSONObject(TAG_DIV).getString("name").contentEquals(wantedTeam)) {
                for (int j = 0; j < leagueTeams.length(); j++) {
                    //Log.d("team", "GetTeamInfo: " +leagueTeams.getJSONObject(i).getJSONObject(TAG_TEAM).getString("name"));
                    if (leagueTeams.getJSONObject(j).getJSONObject(TAG_TEAM).getString("name").equals(wantedTeam)) {
                        selectedDiv = array.getJSONObject(i);
                        //Log.d("DIVISION", "GetTeamInfo: " + selectedDiv.toString());
                    }
                }
            }
            //Log.d("DIVISION", "GetTeamInfo: " + selectedDiv.toString());
            teamDiv = selectedDiv.getJSONObject(TAG_DIV).getString("name");
            teamConf = selectedDiv.getJSONObject(TAG_CONF).getString("name");
            //Log.d("team", "GetTeamInfo: " + teamDiv + ", " + teamConf);

            JSONArray tt = selectedDiv.getJSONArray(TAG_TEAMRECORDS);
            for (int j = 0; j < tt.length(); j++) {
                JSONObject teamRecords = tt.getJSONObject(j);
                JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                String name = team.getString(TAG_NAME);
                String id = team.getString(TAG_ID);
                //Log.d("Teamname", "GetTeamInfo: " + name);

                if (name.contentEquals(wantedTeam)) {
                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);
                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);
                    String goalsFor = teamRecords.getString(TAG_GF);
                    String goalsAgainst = teamRecords.getString(TAG_GA);
                    int difference = Integer.valueOf(goalsFor) - Integer.valueOf(goalsAgainst);
                    String goalDiff = String.valueOf(difference);
                    String divRank = teamRecords.getString(TAG_DIVRANK);
                    String confRank = teamRecords.getString(TAG_CONFRANK);

                    //Log.d("points", "GetTeamInfo: " + points);

                    String uri = "@drawable/l"+ id;
                    int resId = getResources().getIdentifier(uri, null, getPackageName());
                    Drawable drawable = getResources().getDrawable(resId);

                    slogo.setImageDrawable(drawable);
                    sTeamName.setText(name);
                    sGp.setText(gamesPlayed);
                    sWins.setText(wins);
                    sLosses.setText(losses);
                    sOtLosses.setText(otLosses);
                    sPoints.setText(points);
                    sGoalsFor.setText(goalsFor);
                    sGoalsAgainst.setText(goalsAgainst);
                    sDifference.setText(goalDiff);
                    sDivRank.setText(divRank);
                    sConfRank.setText(confRank);
                    sDivision.setText(teamDiv);
                    sConference.setText(teamConf);

//                    HashMap<String, String> teamMap = new HashMap<>();
//                    teamMap.put(TAG_TEAM, name);
//                    teamMap.put(TAG_GP, gamesPlayed);
//                    teamMap.put(TAG_WINS, wins);
//                    teamMap.put(TAG_LOSSES, losses);
//                    teamMap.put(TAG_OTLOSSES, otLosses);
//                    teamMap.put(TAG_POINTS, points);
//                    teamInfo.add(teamMap);
                }


            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return teamInfo;
    }*/
}
