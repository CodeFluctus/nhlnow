package com.example.nhlnow;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.nhlnow.CoreClasses.NHLLeague;
import com.example.nhlnow.CoreClasses.NHLTeam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class StatsByTeamsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, VolleyConnector.VolleyConnectorInterface {

    private static final String TAG_RECORDS = "records";
    private static final String TAG_DIV = "division";
    private static final String TAG_CONF = "conference";
    private static final String TAG_TEAMRECORDS = "teamRecords";
    private static final String TAG_TEAM = "team";
    private static final String TAG_NAME = "name";
    private static final String TAG_LEAGUERECORDS = "leagueRecord";
    private static final String TAG_POINTS = "points";
    private static final String TAG_WINS = "wins";
    private static final String TAG_LOSSES = "losses";
    private static final String TAG_OTLOSSES = "ot";
    private static final String TAG_GP = "gamesPlayed";
    private static final String TAG_DIV_METRO = "Metropolitan";
    private static final String TAG_DIV_ATLANTIC = "Atlantic";
    private static final String TAG_DIV_CENTRAL = "Central";
    private static final String TAG_DIV_PACIFIC = "Pacific";
    private static final String TAG_CONF_EASTERN = "Eastern";
    private static final String TAG_CONF_WESTERN = "Western";

    private ListView listView;
    private String url = "https://statsapi.web.nhl.com/api/v1/standings";
    private ArrayList<HashMap<String, String>> dataList = new ArrayList<>();
    JSONArray jsonResponse = null;
    private Button buttonConference;
    private Button buttonDiv;
    private NHLLeague nhlLeague;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats_by_teams);

        listView = findViewById(R.id.AreaResponseListView);
        buttonConference = findViewById(R.id.buttonConf);
        buttonDiv = findViewById(R.id.buttonDiv);

        buttonConference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentConf = new Intent(StatsByTeamsActivity.this, StatsByConferenceTeamsActivity.class);
                startActivity(intentConf);
            }
        });
        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentConf = new Intent(StatsByTeamsActivity.this, StatsByDivisionTeamActivity.class);
                startActivity(intentConf);
            }
        });
/*
        // Käynnistetään tiedonhaku Volleyn avulla
        JsonObjectRequest dataRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    // luodaan array joka sisältää tilastotiedot
                    jsonResponse = response.getJSONArray(TAG_RECORDS);

                    //dataList = GetDivisionTeams(jsonResponse, TAG_DIV_ATLANTIC );
                    //dataList = GetConferenceTeams(jsonResponse, TAG_CONF_EASTERN );
                    dataList = GetLeagueTeams(jsonResponse);
                    Log.d("dataList", "size: "+ dataList.size());

                    // sortataan joukkueet pistejärjestykseen suurimmasta pienimpään
                    Collections.sort(dataList, new Comparator<HashMap<String, String>>() {
                        @Override
                        public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                            return o2.get(TAG_POINTS).compareTo(o1.get(TAG_POINTS));
                        }
                    });
                    //luodaan adapteri, jolla saadaan datalistin sisältämä tieto näkymään näytöllä
                    ListAdapter adapter = new SimpleAdapter(StatsByTeamsActivity.this, dataList, R.layout.list_item, new String[] {TAG_TEAM, TAG_GP, TAG_WINS, TAG_LOSSES, TAG_OTLOSSES, TAG_POINTS}, new int[]{ R.id.teamName,R.id.gamesPlayed, R.id.wins, R.id.losses, R.id.otlosses, R.id.teamPoints} );
                    listView.setAdapter(adapter);

                    Log.d("JPTAG", dataList.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        //Connector-luokassa on määritelty Volleyn toiminta, jonka avulla haetaan Jsondata nettisivulta
        VolleyConnector.getInstance(this).addToRequestQueue(dataRequest);*/
        nhlLeague = NHLLeague.getInstance();
        nhlLeague.orderTeamsByPoints();
        listView.setOnItemClickListener(this);

        //VolleyConnector volleyConnector = VolleyConnector.getInstance(this);
        //volleyConnector.setCallback(this);

        updateListViewOfTeams();
        /*ListAdapter adapter = *///volleyConnector.getAdapterFrom("leagueTeams", this);
        //listView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Log.d(TAG, "onItemClick: " + dataList.get(position).get("areaCode"));

        //NHLLeague nhlLeague = NHLLeague.getInstance();
        String team = nhlLeague.getTeamFromLeagueByIndex(position).getName();//dataList.get(position).get(TAG_TEAM);
        Log.d("selectedTeam", "onItemClick: " + team);
        //String json = jsonResponse.toString();

        //Log.d("ASD",);

        //avataan StatsBySingleTeamActivity Intentin avulla ja lähetetään sille samalla tietona maakoodi
        Intent intent = new Intent(this, StatsBySingleTeamActivity.class);
        intent.putExtra("name", team);
        //intent.putExtra("json", json);

        startActivity(intent);
    }

    @Override
    public void leagueRecordsGathered() {
        updateListViewOfTeams();
    }

    @Override
    public void allPlayerStatsGathered() {

    }

    public void updateListViewOfTeams() {
        //NHLLeague league = NHLLeague.getInstance();
        Log.d("ASD", "NHLLeague size: " + nhlLeague.getNHLLeagueSize());
        ArrayList<HashMap<String, String>> NHLTeamsList = new ArrayList<>();

        for (int i = 0; i < nhlLeague.getNHLLeagueSize(); i++) {
            NHLTeam nhlTeam = nhlLeague.getTeamFromLeagueByIndex(i);

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put(TAG_TEAM, nhlTeam.getName());
            hashMap.put(TAG_GP, Integer.toString(nhlTeam.getGamesPlayed()));
            hashMap.put(TAG_WINS, Integer.toString(nhlTeam.getLeagueRecord().get("wins")));
            hashMap.put(TAG_LOSSES, Integer.toString(nhlTeam.getLeagueRecord().get("losses")));
            hashMap.put(TAG_OTLOSSES, Integer.toString(nhlTeam.getLeagueRecord().get("otLosses")));
            hashMap.put(TAG_POINTS, Integer.toString(nhlTeam.getPoints()));
            NHLTeamsList.add(hashMap);
        }

        /*
        // sortataan joukkueet pistejärjestykseen suurimmasta pienimpään
        Collections.sort(NHLTeamsList, new Comparator<HashMap<String, String>>() {
                    @Override
                    public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                        return o2.get(TAG_POINTS).compareTo(o1.get(TAG_POINTS));
                    }
        });
*/
        String from[] = {TAG_TEAM, TAG_GP, TAG_WINS, TAG_LOSSES, TAG_OTLOSSES, TAG_POINTS};
        int to[] = {R.id.teamName,R.id.gamesPlayed, R.id.wins, R.id.losses, R.id.otlosses, R.id.teamPoints};

        ListAdapter adapter = new SimpleAdapter(this, NHLTeamsList, R.layout.list_item, from, to);

        listView.setAdapter(adapter);
    }



    /*
    public ArrayList<HashMap<String, String>> GetDivisionTeams(JSONArray array, String division) {

        ArrayList<HashMap<String, String>> divTeams = new ArrayList<>();
        JSONObject divisions = new JSONObject();

        try {
            for (int i = 0; i < array.length(); i++) {

                if (array.getJSONObject(i).getJSONObject(TAG_DIV).getString("name").contentEquals(division)) {
                    divisions = array.getJSONObject(i);
                }
            }
                JSONArray tt = divisions.getJSONArray(TAG_TEAMRECORDS);
                for (int j = 0; j < tt.length(); j++) {
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();
                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    divTeams.add(teamMap);
                }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return divTeams;
    }

    public ArrayList<HashMap<String, String>> GetConferenceTeams(JSONArray array, String conference) {

        ArrayList<HashMap<String, String>> confTeams = new ArrayList<>();

        try {
            JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {

                if (array.getJSONObject(i).getJSONObject(TAG_CONF).getString("name").contentEquals(conference)) {
                    conf.put(array.getJSONObject(i));
                }
            }

            JSONArray tt= new JSONArray();
            for (int i = 0; i < conf.length(); i++) {

                tt = conf.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);

                //tallennetaan uuteen JsonArrayhin yhden joukkeen tiedot kerrallaan

                for (int j = 0; j < tt.length(); j++) {
                    Log.d("TT", "" + tt.getJSONObject(j).toString());
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();
                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    confTeams.add(teamMap);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return confTeams;
    }

    public ArrayList<HashMap<String, String>> GetLeagueTeams(JSONArray array) {

        ArrayList<HashMap<String, String>> confTeams = new ArrayList<>();

        try {
            //JSONArray conf = new JSONArray();
            for(int i = 0; i < array.length(); i++ ) {

                //tallennetaan uuteen JsonArrayhin yhden joukkeen tiedot kerrallaan
                JSONArray tt = array.getJSONObject(i).getJSONArray(TAG_TEAMRECORDS);
                Log.d("JJP", "onResponse: " + tt.length());
                for (int j = 0; j < tt.length(); j++) {
                    Log.d("TT", "" + tt.getJSONObject(j).toString());
                    JSONObject teamRecords = tt.getJSONObject(j);
                    JSONObject team = teamRecords.getJSONObject(TAG_TEAM);
                    String name = team.getString(TAG_NAME);

                    JSONObject leagueRecords = teamRecords.getJSONObject(TAG_LEAGUERECORDS);
                    String wins = leagueRecords.getString(TAG_WINS);
                    String losses = leagueRecords.getString(TAG_LOSSES);
                    String otLosses = leagueRecords.getString(TAG_OTLOSSES);

                    String points = teamRecords.getString(TAG_POINTS);
                    String gamesPlayed = teamRecords.getString(TAG_GP);

                    HashMap<String, String> teamMap = new HashMap<>();
                    teamMap.put(TAG_TEAM, name);
                    teamMap.put(TAG_GP, gamesPlayed);
                    teamMap.put(TAG_WINS, wins);
                    teamMap.put(TAG_LOSSES, losses);
                    teamMap.put(TAG_OTLOSSES, otLosses);
                    teamMap.put(TAG_POINTS, points);
                    confTeams.add(teamMap);
                    Log.d("List", "GetLeagueTeams: "+ confTeams.size());
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
        return confTeams;
    }*/
}

